package controller;

import api.IMovingViolationsManager;
import model.data_structures.LinkedList;
import model.logic.MovingViolationsManager;
import model.vo.VOMovingViolations;
import model.data_structures.*;

public class Controller {

	private static final String movingViolationsFile = "Moving_Violations_Issued_in_January_2018.csv"; 
	
	/**
	 * Reference to the services manager
	 */
	@SuppressWarnings("unused")
	private static IMovingViolationsManager  manager = new MovingViolationsManager();
	
	public static void loadMovingViolations() 
	{
		manager.loadMovingViolations(movingViolationsFile);
	}
	
	public static LinkedList <MovingViolations> getMovingViolationsByViolationCode (String violationCode) 
	{
		return manager.getMovingViolationsByViolationCode(violationCode);
	}
	
	public static LinkedList <MovingViolations> getMovingViolationsByAccident(String accidentIndicator) 
	{		
		return manager.getMovingViolationsByAccident(accidentIndicator);	
	}
}
