//Tomado de https://gist.github.com/lobster1234/5037064

//Editado por: Arturo Rubio

package model.data_structures;

public class Node <T>
{

	private T value;
	
	private Node<T> next;
	
	private Node<T> previous;
	
	public Node (T value)
	{
		this.value = value;
		next = null;
		previous = null;
	}
	
	public void setNext(Node<T> next)
	{
		this.next = next;
	}
	
	public Node<T> getNext()
	{
		return next;
	}
	
	public void setPrevious(Node<T> prev)
	{
		this.previous = prev;
	}
	
	public Node<T> getPrevious()
	{
		return previous;
	}
	
	public T getValue()
	{
		return value;
	}
	
	public void setValue(T value)
	{
		this.value = value;
	}
}
