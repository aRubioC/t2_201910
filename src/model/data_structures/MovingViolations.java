package model.data_structures;

public class MovingViolations 
{

	private int objectId;
	
	private String location;
	
	private String ticketIssueDate;
	
	private int totalPaid;
	
	private String accidentIndicator;
	
	private String violationDescription;
	
	public MovingViolations(int id, String loc, String date, int paid, String acc, String viol) 
	{
		objectId = id;
		
		location = loc;
		
		ticketIssueDate = date;
		
		totalPaid = paid;
		
		accidentIndicator = acc;
		
		violationDescription = viol;
	}
	
	public int getObjectId() {
		// TODO Auto-generated method stub
		return objectId;
	}	
	
	
	/**
	 * @return location - Dirección en formato de texto.
	 */
	public String getLocation() {
		// TODO Auto-generated method stub
		return location;
	}

	/**
	 * @return date - Fecha cuando se puso la infracción .
	 */
	public String getTicketIssueDate() {
		// TODO Auto-generated method stub
		return ticketIssueDate;
	}
	
	/**
	 * @return totalPaid - Cuanto dinero efectivamente pagó el que recibió la infracción en USD.
	 */
	public int getTotalPaid() {
		// TODO Auto-generated method stub
		return totalPaid;
	}
	
	/**
	 * @return accidentIndicator - Si hubo un accidente o no.
	 */
	public String  getAccidentIndicator() {
		// TODO Auto-generated method stub
		return accidentIndicator;
	}
		
	/**
	 * @return description - Descripción textual de la infracción.
	 */
	public String  getViolationDescription() {
		// TODO Auto-generated method stub
		return violationDescription;
	}
}
