package model.logic;

import api.IMovingViolationsManager;
import model.data_structures.LinkedList;
import model.data_structures.MovingViolations;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
public class MovingViolationsManager implements IMovingViolationsManager 
{

	private LinkedList<MovingViolations> moving;
	
	
	public void loadMovingViolations(String movingViolationsFile)
	{
		// TODO Auto-generated method stub
		Path path = FileSystems.getDefault().getPath("data", movingViolationsFile);
		Reader reader;
		try 
		{
			reader = Files.newBufferedReader(path);
			
			CSVParser parser = new CSVParserBuilder().withSeparator(',').withIgnoreQuotations(true).build();
				 
			CSVReader csvReader = new CSVReaderBuilder(reader).withSkipLines(1).withCSVParser(parser).build();
			
			moving = new LinkedList<MovingViolations>();
			ArrayList list = new ArrayList();
		    String[] line;
		    while ((line = csvReader.readNext()) != null) 
		    {
		    	if(line[0].equals("") || line[2].equals("") || line[13].equals("") || line[9].equals("") || line[12].equals("") || line[14].equals(""))
		    	{
		    		list.add(line);
		    	}
		    	else
		    	{
			    	MovingViolations movet = new MovingViolations(Integer.parseInt(line[0]), line[2], line[13], Integer.parseInt(line[9]), line[12], line[14]);
			        moving.add(movet);
		    	}
		    	
		    }
		    reader.close();
		    csvReader.close();
		    
		    System.out.println("Se cargaron exitosamente " + moving.getSize() + " registros");
		    System.out.println("No se pudieron cargar " + list.size() + " registros");
		    
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
		
	}

		
	@Override
	public LinkedList <MovingViolations> getMovingViolationsByViolationCode (String violationCode) 
	{
		LinkedList<MovingViolations> ans = new LinkedList<MovingViolations>();
		
		Iterator it = moving.iterator();
		
		while(it.hasNext())
		{
			MovingViolations mov = (MovingViolations) it.next();
			if(mov.getViolationDescription().equals(violationCode))
			{
				ans.add(mov);
			}
		}
		
		return ans;
	}

	@Override
	public LinkedList<MovingViolations> getMovingViolationsByAccident(String accidentIndicator) 
	{
		// TODO Auto-generated method stub
		
LinkedList<MovingViolations> ans = new LinkedList<MovingViolations>();
		
		Iterator it = moving.iterator();
		
		while(it.hasNext())
		{
			MovingViolations mov = (MovingViolations) it.next();
			if(mov.getAccidentIndicator().equals(accidentIndicator))
			{
				ans.add(mov);
			}
		}
		
		return ans;
	}	


}
